# build before with docker build -t claraviz:latest . or Dockerfile right-click in VSCode -> build <3
docker run -it --rm \
    --gpus=all \
    --volume /home/t/temp:/home/jovyan/work/data \
    -p 8888:8888 \
    --name tigre \
    tigre:latest