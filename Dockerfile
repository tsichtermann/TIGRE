FROM nvidia/cuda:11.4.2-base-ubuntu20.04
#FROM nvcr.io/nvidia/pytorch:21.12-py3

RUN apt update \
    && apt install -y --no-install-recommends --fix-missing \
    git \
    g++ gcc \
    python3.8 python3-pip python3-setuptools python3-dev \
    nvidia-cuda-toolkit 

WORKDIR /home/jovyan/work/
RUN git clone https://github.com/CERN/TIGRE

RUN python3 -m pip install --upgrade pip
RUN pip3 install --no-cache-dir cython numpy

WORKDIR /home/jovyan/work/TIGRE/Python
RUN python3 setup.py install

RUN pip3 install jupyter && pip3 install jupyterlab

WORKDIR /home/jovyan/work

ENTRYPOINT ["jupyter", "lab","--ip=0.0.0.0","--allow-root"]